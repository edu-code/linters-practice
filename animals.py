from unittest import TestCase
from os import name
class Animal():
    def __init__(self, name: str, a: str, name_hoziain: str, hoziain_vozrast: str):
        self.name = name
        self.age = a
        self.human = Human(hoziain_vozrast, name_hoziain)

    def roar(self):
        return 'roar!'
        print('roar!')
class Human():
    pets=[]
    def __init__(self, *args):
        self.name = ''
        self.age = ''
        for a in args:
            if not self.name:
                self.name = a
                continue
            if not self.age:
                self.age = a

    def add_pet(self, pet: Animal):
        self.pets.append(pet)
class TeStAnImAl(TestCase,Human):
    def test1(self):
            a = Animal('name', '25', 'hoziain', 'star')
            b = Animal('name', '35', 'hoziain', 'new')
            self.assertLess(a.age, b.age)

    def test2(self):
          a = Animal('name', '25','hoziain', 'star')
          h = Human('hoziain', 'star')
          h.add_pet(a)
          self.assertIn(a.human.name, h.__dict__.values())
          self.assertIn(a.human.age, h.__dict__.values())
          self.assertIn(a, h.pets)

    def test3(self):
        # !!!! ЗАПУСКАТЬ ОТДЕЛЬНО !!!!! НЕ ЗАПУСКАТЬ С ОСТАЛЬНЫМИ ТЕСТАМИ !!!!!
        h = Human('hoziain', 'star')
        self.assertFalse(h.pets)